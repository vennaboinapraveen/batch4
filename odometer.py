class Odometer:
    
    def _init_(self, digit, reading, nth_reading):
        self.START = '123456789'[:digit]
        self.END = '123456789'[-digit:]
        self.reading = str(reading)
        self.current = self.reading
        self.nthreading = nth_reading
        self.digit = digit

    def zero_check(self):
        if '0' in self.reading:
            return True
        return False

    def ascending_order_check(self):
        for i in range(len(self.reading)):
            if i < (len(self.reading)-1) and self.reading[i] >= self.reading[i+1]:
                return False
        return True
    
    def next_reading(self):
        k = Odometer.ascending_order_check(self)
        while(k):
            self.reading = str(int(self.reading) + 1)
            if Odometer.ascending_order_check(self):
                return self.reading[:self.digit]
        return 'reading is incorrrect'

    def previous_reading(self):
        k = Odometer.ascending_order_check(self)
        while(k):
            self.reading = str(int(self.reading) - 1)
            if Odometer.ascending_order_check(self):
                return self.reading
        return 'reading is incorrect'

    def nth_reading(self):
        nth = 1
        k = Odometer.ascending_order_check(self)
        while(k):
            if self.reading == self.END:
                self.reading = self.START
                self.reading = str(int(self.reading) + 1)
                if Odometer.ascending_order_check(self):
                    nth = nth + 1
                    if nth == int(self.nthreading):
                        return self.reading[:self.digit]
        return 'no reading'

    def _repr_(self):
        return f'{self.START} << {self.current} >> {self.END}' 

    def _str_(self):
        return f'''reading: {self.current}
        \nstarting_reading: {self.START}
        \nending_reading: {self.END}
        \ncheck_zero: {Odometer.zero_check(self)}
        \nascending_check: {Odometer.ascending_order_check(self)}
        \nnext_reading: {Odometer.next_reading(self)}
        \nprevious_reading: {Odometer.previous_reading(self)}
        \nnth_reading: {Odometer.nth_reading(self)}'''
    
vehicle1 = Odometer(3,6789,6)
print(vehicle1.zero_check())
print(vehicle1.ascending_order_check())
print(vehicle1.next_reading())
 
vehicle2 = Odometer(4,6780,7)
print(vehicle2.zero_check())
print(vehicle2.ascending_order_check())
print(vehicle2.next_reading())